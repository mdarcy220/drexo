# Drexo

Drexo is a framework for **D**istributed, **R**eproducible **Ex**perimental **O**utcomes. It is currently in very early alpha stages and should not be relied upon for production use.

## Project Goals

In short, Drexo aims to manage the execution of processes for software-based experiments. Some of the main goals for Drexo include:

- Dynamic job queuing (e.g., depending on the output of job 1, decide what to run for job 2)
- Tracking information about each experiment run (enough info to know exactly what was run so the experiment could be reproduced), such as:
    - The exact command that was run (e.g., `python my_code.py --data data_dir/`)
    - The environment variables present when the command was invoked
    - Checksums of the code and data files used in the experiment
    - The output and exit status of the command
- Distributed job queuing with automatic resource mangement
    - For example: run 10 experiments, where 5 of them require at least one available GPU to run, in an environment where there are two servers and one of the servers has two GPUs
- Easier hyperparameter search
- Easier aggregation of the results of many experiments
