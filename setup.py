import setuptools

with open("README.md", "r") as fh:
	long_description = fh.read()

setuptools.setup(
	name="drexo",
	version="0.1.0",
	author="Mike D'Arcy",
	author_email="author@example.com",
	description="Distributed job queue for reproducible experiments",
	long_description=long_description,
	long_description_content_type="text/markdown",
	license='Not currently licensed (no rights granted)',
	url="https://gitlab.com/mdarcy220/drexo",
	packages=setuptools.find_packages(),
	classifiers=[
		"Topic :: Scientific/Engineering",
		"Development Status :: 2 - Pre-Alpha",
		"Programming Language :: Python :: 3",
		"License :: Other/Proprietary License",
		"Operating System :: POSIX :: Linux",
	],
)
